var end;

function changeDate() {
    data = prompt("Ktorego dnia miesiaca masz wyplate?","");
    if (data != "" && data != null) {
        setCookie("data", data);
    }
    countdown();
}

function setCookie(key, value) {
    document.cookie = key + "=" + value + ";";
}

function getCookie(key) {
    var name = key + "=";
    var charArr = document.cookie.split(";");
    for (var i=0; i < charArr.length; i++){
        var char = charArr[i];
        while (char.charAt(0) == " ") char = char.substring(1);
        if (char.indexOf(name) != -1) return char.substring(name.length, char.length);
    }
    return "";
}

function searchCookie(key) {
    var data = getCookie(key);
    if (data != "") {
        countdown();
    } else {
        changeDate();
    }
}

function countdown() {

    var date;
    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour *24
    var timer;

    //searchCookie("data");
    end = new Date(); end.setDate(getCookie("data"));// set expiry date and time..

    function showRemaining()
    {

        var now = new Date();
        var distance = end - now;
        if (now.getDate() == getCookie("data")) {
            $("#text2").hide();
            $("#countdown").hide();
            document.getElementById('decide').innerHTML =  "TAK";
            return; // break out of the function so that we do not update the counters with negative values..
        } else if (now.getDate() > getCookie("data")) {
            $("#text1").hide();
            $("#text2").show();
            $("#countdown").show();
            document.getElementById('decide').innerHTML =  "TAK";
            if (now.getMonth() == 11) {
                end.setMonth(0);
                end.setFullYear(now.getFullYear() + 1);
            } else {
                end.setMonth(now.getMonth() + 1);
            }
        } else {
            $("#text1").show();
            $("#text2").hide();
            $("#countdown").show();
            document.getElementById('decide').innerHTML =  "NIE";
        }
        var days = Math.floor(distance / _day);
        var hours = Math.floor( (distance % _day ) / _hour );
        var minutes = Math.floor( (distance % _hour) / _minute );
        var seconds = Math.floor( (distance % _minute) / _second );
     
        document.getElementById('countdown').innerHTML =  days + " dni  ";
        document.getElementById('countdown').innerHTML +=  hours+ " godzin  ";
        document.getElementById('countdown').innerHTML +=  minutes+ " minut  ";
        document.getElementById('countdown').innerHTML += seconds+ " sekund  ";

    }

    timer = setInterval(showRemaining, 1000);
}

window.onload = countdown();
